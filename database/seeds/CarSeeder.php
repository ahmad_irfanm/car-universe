<?php

use Illuminate\Database\Seeder;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $cars = [
            [
                'name' => 'BMW 8 Series',
                'price' => 4290000000,
                'stock' => 5
            ],
            [
                'name' => 'BMW 6 Series',
                'price' => 1810000000,
                'stock' => 20
            ],
            [
                'name' => 'BMW 3 Series',
                'price' => 1280000000,
                'stock' => 34
            ],
            [
                'name' => 'BMW X6',
                'price' => 2150000000,
                'stock' => 90
            ],
            [
                'name' => 'BMW Z4',
                'price' => 2660000000,
                'stock' => 55
            ]
        ];

        foreach ($cars as $car) {
            \App\Car::create($car);
        }
    }
}
