<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

class CustomLoginController extends Controller
{
    //
    function __construct (User $user) {
        $this->user = $user;
    }

    public function login (Request $request) {
        // rules
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // field validation
        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        try {

            // check email first
            $user = $this->user->where('email', $request->email)->first();
            if (!$user)
                return response()->json([
                    'message' => 'Email Not Found',
                ], 401);

            // check password
            if (!app('hash')->check($request->password, $user->password)) {
                return response()->json([
                    'message' => 'Password Incorrect',
                ], 401);
            }

            // create token
            $token = bcrypt($user->email);

            // update remember token field
            $user->update([
                'remember_token' => $token,
            ]);

            // message login successful
            return response()->json([
                'message' => 'Login successful',
                'data' => [
                    'user' => $user,
                    'token' => $token,
                ]
            ], 200);

        } catch (\Exception $e) {

            // message created car error
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);

        }
    }

    public function logout (Request $request) {

        // check token
        $user = $this->user->where('remember_token', $request->token)->first();
        if (!$user)
            return response()->json([
                'message' => 'Token invalid',
            ], 401);

        try {

            // set remember token to null
            $user->update([
                'remember_token' => null
            ]);

            // message created purchase successful
            return response()->json([
                'message' => 'Logout successful'
            ], 200);

        } catch (\Exception $e) {

            // message created car error
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);

        }


    }
}
