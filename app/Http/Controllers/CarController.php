<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use App\Purchase;
use Validator;

class CarController extends Controller
{
    //
    function __construct (Car $car, Purchase $purchase) {
        $this->car = $car;
        $this->purchase = $purchase;
    }

    public function index () {
        $cars = $this->car->orderBy('name', 'asc')->get();

        return response()->json([
            'message' => 'Cars loaded successful',
            'data' => [
                'cars' => collect($cars)->map(function ($car) {
                    return ['car' => $car];
                })
            ],
        ], 200);
    }

    public function show ($id) {
        $car = $this->car->find($id);

        return response()->json([
            'message' => 'Car loaded successful',
            'data' => [
                'car' => $car
            ],
        ], 200);
    }

    public function store (Request $request) {

        // rules
        $validator = Validator::make($request->all(), [
           'name' => 'required',
           'price' => 'required|numeric',
           'stock' => 'required|numeric',
        ]);

        // field validation
        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        try {

            // insert car
            $car = $this->car->create([
                'name' => $request->name,
                'price' => $request->price,
                'stock' => $request->stock,
            ]);

            // message created car successful
            return response()->json([
                'message' => 'Car created successful',
                'data' => [
                    'car' => $car,
                ]
            ], 200);

        } catch (\Exception $e) {

            // message created car error
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);

        }
    }

    public function update (Request $request, Car $car) {
        // rules
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
        ]);

        // field validation
        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        try {

            // update car
            $car->update([
                'name' => $request->name,
                'price' => $request->price,
                'stock' => $request->stock,
            ]);

            // message created car successful
            return response()->json([
                'message' => 'Car updated successful',
                'data' => [
                    'car' => $car,
                ]
            ], 200);

        } catch (\Exception $e) {

            // message created car error
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);

        }
    }

    public function destroy (Car $car) {
        try {

            // delete car
            $car->delete();

            // message deleted car successful
            return response()->json([
                'message' => 'Car deleted successful',
            ], 200);

        } catch (\Exception $e) {

            // message created car error
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);

        }
    }
}
