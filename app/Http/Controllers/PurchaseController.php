<?php

namespace App\Http\Controllers;

use App\Mail\SendInvoiceNotification;
use App\Purchase;
use App\Car;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class PurchaseController extends Controller
{
    //
    function __construct (Purchase $purchase, Car $car) {
        $this->purchase = $purchase;
        $this->car = $car;
    }

    public function index () {
        $purchases = $this->purchase->orderBy('created_at', 'desc')->with(['car'])->get();

        return response()->json([
            'message' => 'Purchase loaded successful',
            'data' => [
                'purchases' => collect($purchases)->map(function ($purchase) {
                    return ['purchase' => $purchase];
                })
            ],
        ], 200);
    }

    public function store (Request $request) {

        // rules
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'car_id' => 'required|numeric',
        ]);

        // field validation
        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        // car not found
        $car = $this->car->find($request->car_id);
        if (!$car)
            return response()->json([
                'message' => 'ID Car Not Found',
            ], 401);

        $request->car = $car;
        // send email here
        Mail::send(new SendInvoiceNotification($request));

        try {

            // insert purchase
            $purchase = $this->purchase->create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'car_id' => $car->id,
            ]);

            // message created purchase successful
            return response()->json([
                'message' => 'Purchase created successful',
                'data' => [
                    'purchase' => $purchase,
                ]
            ], 200);

        } catch (\Exception $e) {

            // message created car error
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);

        }
    }

    public function destroy (Purchase $purchase) {
        try {

            // delete purchase
            $purchase->delete();

            // message deleted purchase successful
            return response()->json([
                'message' => 'Purchase removed successful',
            ], 200);

        } catch (\Exception $e) {

            // message created car error
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);

        }
    }
}
