<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Purchase;
use App\Car;


class DashboardController extends Controller
{
    //
    function __construct (User $user, Purchase $purchase, Car $car) {
        $this->user = $user;
        $this->purchase = $purchase;
        $this->car = $car;
    }

    public function statistic_today () {
        $now = date('Y-m-d');

        return $this->statistic($now, $now);
    }

    public function statistic_week () {
        $end = date('Y-m-d');
        $start = date('Y-m-d', strtotime('-7 days', strtotime($end)));


        return $this->statistic($start, $end);
    }

    public function statistic ($start, $end) {

        // in one day
        if ($start == $end) {
            $yesterday = date('Y-m-d', strtotime('-1 days', strtotime($start)));

            $purchases = $this->purchase->whereDate('created_at', $start)->get();
            $purchases_today = $purchases->count();
            $purchases_yesterday = $this->purchase->whereDate('created_at', $yesterday)->count();

            $difference = $purchases_today - $purchases_yesterday;
            $percentage = ($difference / $purchases_today) * 100;

            $cars = $this->car->with(['purchases' => function ($purchase) use ($start) {
                $purchase->whereDate('created_at', $start)->get();
            }])->get()->map(function ($car) {
                $car->purchase_count = $car->purchases->count();
                unset($car->purchases);
                return $car;
            })->sortByDesc('purchase_count')->first();

            $purchase_count = $purchases_today;

        } else {
            $percentage = null;
            $end = date('Y-m-d', strtotime('+1 days', strtotime($end)));

            $purchases = $this->purchase->whereBetween('created_at', [$start, $end])->get();

            $cars = $this->car->with(['purchases' => function ($purchase) use ($start, $end) {
                $purchase->whereBetween('created_at', [$start, $end])->get();
            }])->get()->map(function ($car) {
                $car->purchase_count = $car->purchases->count();
                unset($car->purchases);
                return $car;
            })->sortByDesc('purchase_count')->first();

            $purchase_count = $purchases->count();
        }

        $total = 0;
        foreach ($purchases as $purchase) {
            $total += $purchase->car->price;
        }

        return response()->json([
            'message' => 'Statistic loaded successful',
            'data' => [
                'car' => $cars,
                'percentage' => number_format($percentage, 1),
                'purchase_count' => $purchase_count,
                'total' => $total,
            ],
        ], 200);
    }
}
