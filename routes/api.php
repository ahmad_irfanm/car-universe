<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'CustomLoginController@login');
Route::get('/logout', 'CustomLoginController@logout');
Route::get('/statistic/today', 'DashboardController@statistic_today');
Route::get('/statistic/week', 'DashboardController@statistic_week');
Route::apiResource('/cars', 'CarController');
Route::apiResource('/purchases', 'PurchaseController');
