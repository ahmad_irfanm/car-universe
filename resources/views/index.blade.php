@extends('layout.master')

@section('content')
    <div>
        <router-view></router-view>
    </div>
@endsection
