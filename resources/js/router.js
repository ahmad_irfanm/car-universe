import Vue from 'vue'
import VueRouter from 'vue-router'

import DashboardContainer from './containers/DashboardContainer';
import CarListContainer from './containers/CarListContainer';
import PurchaseListContainer from './containers/PurchaseListContainer';
import LoginContainer from './containers/LoginContainer';
import NotFoundContainer from './containers/NotFoundContainer';

Vue.use(VueRouter)

const routes = [
    { path: '/', component: DashboardContainer, meta: {auth: true} },
    { path: '/login', component: LoginContainer },
    { path: '/car-list', component: CarListContainer, meta: {auth: true} },
    { path: '/purchase-list', component: PurchaseListContainer, meta: {auth: true} },
    { path: '/*', component: NotFoundContainer },
]

const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.auth)) {
        if (!localStorage.getItem('token')) {
            next({
                path: '/login',
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})

export default router;

